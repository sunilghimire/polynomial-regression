# Polynomial Regression

Polynomial Regression is a form of linear regression in which the relationship between the independent variable X and dependent variable y is modeled as an nth degree polynomial that provides the best approximation of the relationship X and y.